---
title: "Verodata-harjoitustyö"
author: samivar
date: "Päivitetty: `r Sys.time()`"
output:
  html_document:
    toc: true
    number_sections: true
    theme: united
    code_folding: hide
    toc_float:
      collapsed: false
      smooth_scroll: false
---


```{r setup, include=FALSE}
library(knitr)
opts_chunk$set(list(echo=TRUE, # kaikkiin koodichunkkeihin tulee koodia. Mieti aina onko tarpeen, mutta html-dokkarissa voit käyttää YAML:ssa code_folding: hide
                    eval=TRUE, # kaikki koodikimpaleet arvioidaan
                    cache=FALSE, # koodikimpaleista ei luoda välimuistia. Raskaammissa projekteissa tätä kannattaa käyttää
                    warning=FALSE, # funktioiden varoituksia ei printata outputtiin 
                    message=FALSE, # funktioiden viestejä ei printata outputtiiin
                    fig.width=8, # kuvien oletuskorkeus
                    fig.height=8) # kuvien oletusleveys 
               )
# joitain optioita
options(scipen=999) # outputteihin tavanomaiset luvut tieteellisen merkinnäin sijaan
```

# Laitetaan otsikko tähän

  **Tehtävä**  
Tarkoituksena on kuvata:  
- Kuinka paljon eri tuloryhmät ovat keskimäärin maksaneet veroa tuloistaan?  
- Millaiset erot tuloryhmien välillä on keskimääräisissä ansio- ja pääomatuloissa?  

Ladataan paketit:
```{r}
library(pxweb)
library(dplyr)
library(tidyverse)
library(scales)
library(ggplot2)
library(reshape2)
```


# Toinen tähän

Ladataan tulo- ja verodatat:  

``` {r}
# kääritään datan hakeminen ehtoon
if (!file.exists("./datakansio/verot.RDS")){
  dir.create("./datakansio", showWarnings = TRUE)
  tulot <- 
  get_pxweb_data(url = "http://vero2.stat.fi/PXWeb/api/v1/fi/Vero/Henkiloasiakkaiden_tuloverot/lopulliset/Tulot/011_tulot.px",
                 dims = list(Verovuosi = c('*'),
                             Erä = c('*'),
                             Alue = c('*'),
                             Tulonsaajaryhmä = c('*'),
                             Tunnusluvut = c('*')),
                 clean = TRUE)
saveRDS(tulot, "./datakansio/tulot.RDS")
 
verot <- 
  get_pxweb_data(url = "http://vero2.stat.fi/PXWeb/api/v1/fi/Vero/Henkiloasiakkaiden_tuloverot/lopulliset/Verot%20ja%20maksut/031_verot_ja_maksut.px",
                 dims = list(Verovuosi = c('*'),
                             Erä = c('*'),
                             Alue = c('*'),
                             Tulonsaajaryhmä = c('*'),
                             Tunnusluvut = c('*')),
                 clean = TRUE)
saveRDS(verot, "./datakansio/verot.RDS")
}

tulot <- readRDS("./datakansio/tulot.RDS")
verot <- readRDS("./datakansio/verot.RDS")
```


Siivotaan dataa:
```{r}
tulot2 <- filter(tulot, Alue %in% "Koko maa", Erä %in% c("4 - Ansiotulot yhteensä","5 - Pääomatulot yhteensä","3 - Veronalaiset tulot yhteensä"), Tunnusluvut %in% c("Summa, euroa","Keskiarvo"))
verot2 <- filter(verot, Alue %in% "Koko maa", Erä %in% c("1. Verot ja veronluonteiset maksut yhteensä"), Tunnusluvut %in% c("Summa, euroa","Keskiarvo"))
```
  
Nimetään sarakkeet uudelleen, yhdistetään ja levitetään data:
```{r}
tulot2 %>% spread(key = Erä, value = values) -> tulot2spr
verot2 %>% spread(key = Erä, value = values) -> verot2spr
full_join(tulot2spr, verot2spr) -> tuljaver
# laitetaan pari riviä näkyville
knitr::kable(head(tuljaver))
```
  
Siivotaan lisää ja luodaan uusi muuttuja, veroprosentti:
```{r}
tuljaver$Alue <- NULL 
names(tuljaver)[names(tuljaver) == '5 - Pääomatulot yhteensä'] <- 'Pääomatulot'
names(tuljaver)[names(tuljaver) == '4 - Ansiotulot yhteensä'] <- 'Ansiotulot'
names(tuljaver)[names(tuljaver) == '3 - Veronalaiset tulot yhteensä'] <- 'Veronal.tulot'
names(tuljaver)[names(tuljaver) == '1. Verot ja veronluonteiset maksut yhteensä'] <- 'Verot.yht'
tuljaver$Tulonsaajaryhmä <- as.character(tuljaver$Tulonsaajaryhmä)
tuljaver$Tulonsaajaryhmä <- replace(tuljaver$Tulonsaajaryhmä, tuljaver$Tulonsaajaryhmä=="Osakeyhtiöiden pääosakkaat, avointen yhtiöiden ja kommandiittiyhtiöiden vastuunalaiset yhtiömiehet", "Osakas")  
tuljaver$Tulonsaajaryhmä <- replace(tuljaver$Tulonsaajaryhmä, tuljaver$Tulonsaajaryhmä=="Elinkeinonharjoittaja", "Elinkeinonharj.")
tuljaver$Tulonsaajaryhmä <- replace(tuljaver$Tulonsaajaryhmä, tuljaver$Tulonsaajaryhmä=="Maatalouden harjoittaja", "Maatalouden harj.")
tuljaver$Tulonsaajaryhmä <- factor(tuljaver$Tulonsaajaryhmä, levels = c("Elinkeinonharj.","Eläkeläinen","Kuolinpesä","Maatalouden harj.","Osakas","Palkansaaja","Muut","Yhteensä"))
tuljaver %>% mutate(Veroprosentti = Verot.yht / Veronal.tulot *100) -> tuljaver
```
  
  
# Kolmas tähän  

## Ja sen alle neljäs
  
Tehdään ensimmäinen kuvio:
```{r}
tuljaver %>% filter(Tunnusluvut %in% "Keskiarvo") %>% ggplot(aes(x=Tulonsaajaryhmä, y=Veroprosentti, fill=Verovuosi)) + geom_bar(stat="identity", position=position_dodge()) + theme(text = element_text(size=12), axis.text.x = element_text(angle=90, hjust=1)) + ggtitle("Yhteenlasketuista tuloista maksettu vero, %")
```
  
Säädetään hieman lisää...
```{r}
tjav_long<-melt(tuljaver,id.vars=c("Verovuosi","Tunnusluvut","Tulonsaajaryhmä"))
names(tjav_long)[names(tjav_long) == 'value'] <- 'Euroa'
names(tjav_long)[names(tjav_long) == 'variable'] <- 'Tuloluokka'
tjav_long$Tuloluokka <- factor(tjav_long$Tuloluokka, levels = c("Pääomatulot", "Veronal.tulot", "Verot.yht", "Ansiotulot", "Veroprosentti"))
tjav_long$Tulonsaajaryhmä <- factor(tjav_long$Tulonsaajaryhmä, levels = c("Elinkeinonharj.","Eläkeläinen","Maatalouden harj.","Osakas","Palkansaaja","Muut","Yhteensä"))
```
  
...ja tehdään toinen kuvio, josta kuolinpesät on jätetty pois.
```{r}  
tjav_long %>% filter(Tunnusluvut %in% "Keskiarvo", Tulonsaajaryhmä %in% c("Elinkeinonharj.","Eläkeläinen","Osakas","Palkansaaja","Maatalouden harj.","Muut","Osakas","Yhteensä"), Tuloluokka %in% c("Ansiotulot","Pääomatulot")) %>%
  ggplot(aes(x=Tulonsaajaryhmä, y=Euroa, fill=Tuloluokka)) + 
  geom_bar(stat="identity", position=position_stack()) + 
  theme(text = element_text(size=12), axis.text.x = element_text(angle=90, hjust=1)) + 
  facet_wrap(~Verovuosi) + 
  # Nimeämisessä suosittelen käyttämään "labs"-funktiota eli 
  # ggtitle("Keskimääräinen ansio tulonsaajaryhmittäin, euroa/vuosi")
  labs(title="Keskimääräinen ansio tulonsaajaryhmittäin, euroa/vuosi",
       fill="Tuloluokka tai \nmuu legendan otsikko",
       caption="Data: vero2.stat.fi")

```





